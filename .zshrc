#!/usr/bin/env zsh

# Exports ######################################################################

export EDITOR="nvim"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export PATH="${PATH}:${HOME}/.local/bin:${HOME}/bin"

export LANG="en_US.UTF-8"           # LANG Defines all locale settings at once, while allowing further individual customization via the LC_* settings below.
export LC_COLLATE="en_US.UTF-8"     # LC_COLLATE Define alphabetical ordering of strings. This affects e.g. output of sorted directory listings.
export LC_CTYPE="en_US.UTF-8"       # LC_CTYPE Define the character-handling properties for the system. This determines which characters are seen as alphabetic, numeric, and so on. This also determines the character set used, if applicable.
export LC_MEASUREMENT="nl_BE.UTF-8" # LC_MEASUREMENT Defines measurement units (metric, US customary, etc.).
export LC_MESSAGES="en_US.UTF-8"    # LC_MESSAGES Programs' localizations stored in /usr/share/locale/ for applications that use a message-based localization scheme (the majority of GNU programs).
export LC_MONETARY="nl_BE.UTF-8"    # LC_MONETARY Defines currency units and formatting of currency-type numeric values.
export LC_NUMERIC="en_US.UTF-8"     # LC_NUMERIC Defines formatting of numeric values which aren't monetary. Affects things such as thousand separator and decimal separator.
export LC_TIME="nl_BE.UTF-8"        # LC_TIME Defines formatting of dates and times.
export LC_PAPER="nl_BE.UTF-8"       # LC_PAPER Defines default paper size.
# LC_ALL    # Overrides all other settings. Don't set, bad practice!

# Settings #####################################################################

# Options
setopt HIST_IGNORE_SPACE # Ignore commands that start with a space
setopt INTERACTIVE_COMMENTS # Allow comments in interactive shell

# History
HISTFILE="${HOME}/.histfile"
HISTSIZE="1000000"
SAVEHIST="1000000"

# Use vi keybindings
bindkey -v

# Fix backspace bug when switching modes: backspace doesn't work after insert -> normal -> insert mode
# https://vi.stackexchange.com/questions/31671/set-o-vi-in-zsh-backspace-doesnt-delete-non-inserted-characters
bindkey "^?" backward-delete-char

# Additional keybindings (detected with `od -c`)
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char
bindkey -M viins '^P' up-history
bindkey -M viins '^N' down-history

# Autocompletion setup
# https://thevaluable.dev/zsh-completion-guide-examples/
# https://zsh.sourceforge.io/Doc/Release/Completion-System.html
zstyle :compinstall filename '/home/martijn/.zshrc'
autoload -Uz compinit
compinit

# Also autocomplete hidden files
_comp_options+=(globdots)

# Define completers
zstyle ':completion:*' completer _extensions _complete _approximate

# Menu for completion
zstyle ':completion:*' menu select

# List entries in rows instead of columns in completion menu
zstyle ':completion:*' list-rows-first true

# Same colors as `ls` in completion menu
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# Group entries in completion menu
zstyle ':completion:*' group-name ''
zstyle ':completion:*' format '--- %d ---'
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}--- %d ---%f'
zstyle ':completion:*:*:*:*:messages' format '%F{purple}--- %d ---%f'
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}--- %d (errors: %e) ---%f'
zstyle ':completion:*:*:*:*:warnings' format '%F{red}--- no matches found ---%f'

# Use vi keys in completion menu
zmodload zsh/complist
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# `//` should be treated as `/`
zstyle ':completion:*' squeeze-slashes true

# Enable caching
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME}/zsh/.zcompcache"

# Colors #######################################################################

# Based on
#   - /usr/share/code/resources/app/extensions/theme-defaults/themes/dark_vs.json
#   - /usr/share/code/resources/app/extensions/theme-defaults/themes/dark_plus.json
#   - /usr/share/code/resources/app/extensions/theme-defaults/themes/dark_modern.json

BLUE="#0000ff"
GREEN="#00ff00"
RED="#ff0000"

BACKGROUND="#1f1f1f"
CLASS_OR_OBJECT_OR_TYPE="#4ec9b0"
COMMENT="#6a9955"
CONSTANT_NUMERIC="#b5cea8"
CONSTANT_OR_ENUM="#4fc1ff"
CONTROL_FLOW="#c586c0"
ERROR="#f85149"
ESCAPED_CHAR="#d7ba7d"
FOREGROUND="#cccccc"
HYPERLINK="#4daafc"
KEYWORD_OR_VARIABLE="#569cd6"
STRING="#ce9178"
SUGGESTION="#888888"

# Startship ####################################################################

export STARSHIP_CONFIG="${XDG_CONFIG_HOME}/starship/starship.toml" # https://github.com/starship/starship/issues/896
eval "$(starship init zsh)"

# Zoxide #######################################################################

eval "$(zoxide init zsh)"
# For completions to work, the above line must be added after compinit is called. You may have to rebuild your completions cache by running rm ~/.zcompdump*; compinit.

# Fzf ############################################################################

eval "$(fzf --zsh)"

export FZF_DEFAULT_OPTS="--no-height --reverse"

# https://github.com/junegunn/fzf/issues/1839
#
# 5x definition of command:
# - Using it as a command
# - Ctrl+T # files or directories
# - Alt+C # change directory
# - cat **<Tab> # files and directories
# - cd **<Tab> # directories

FZF_FD_OPTS="--hidden --follow --exclude '.git'"
# export FZF_DEFAULT_COMMAND= `| fzf` can also work on a list of strings that aren't files

# Using `${=VAR}` to split the string into words according to https://stackoverflow.com/questions/6715388/variable-expansion-is-different-in-zsh-from-that-in-bash

export FZF_CTRL_T_COMMAND="fd ${=FZF_FD_OPTS}"
export FZF_ALT_C_COMMAND="fd --type d ${=FZF_FD_OPTS}"

_fzf_compgen_path() {
    fd ${=FZF_FD_OPTS} . "${1}"
}

_fzf_compgen_dir() {
    fd --type d ${=FZF_FD_OPTS} . "${1}"
}

export FZF_CTRL_R_OPTS="--no-sort"

# EZA ##########################################################################

# https://the.exa.website/docs/colour-themes
# https://github.com/eza-community/eza/blob/main/man/eza_colors.5.md
# https://en.wikipedia.org/wiki/ANSI_escape_code

# Config eza/exa colors to shades of grey instead of a distractful bright colors
EZA_COLORS=""
EZA_COLORS+="lc=38;5;239:lm=38;5;239:" # darker number of links
EZA_COLORS+="sn=38;5;29:sb=38;5;100:" # darker and better contrast for file size
EZA_COLORS+="uu=38;5;239:gu=38;5;239:" # darker username & group
EZA_COLORS+="da=38;5;243:" # darker timestamp

# Darker permissions (shades of grey)
EZA_COLORS+="ur=38;5;240:uw=38;5;244:ux=38;5;248:ue=38;5;248:" # user permissions
EZA_COLORS+="gr=38;5;240:gw=38;5;244:gx=38;5;248:" # group permissions
EZA_COLORS+="tr=38;5;240:tw=38;5;244:tx=38;5;248:" # other permissions
EZA_COLORS+="xa=38;5;24:" # xattr marker ('@')
EZA_COLORS+="xx=38;5;240:" # punctuation ('-')

# Color file sizes by order of magnitude
EZA_COLORS+="nb=38;5;239:"                #  0  -> <1KB : grey
EZA_COLORS+="nk=38;5;29:uk=38;5;100:"     # 1KB -> <1MB : green
EZA_COLORS+="nm=38;5;26:um=38;5;32:"      # 1MB -> <1GB : blue
EZA_COLORS+="ng=38;5;130:ug=38;5;166;1:"  # 1GB -> <1TB : orange
EZA_COLORS+="nt=38;5;160:ut=38;5;197;1:"  # 1TB -> +++  : red

export EZA_COLORS

# Open issues:
#
# - https://github.com/eza-community/eza/issues/443
# - https://github.com/eza-community/eza/issues/315
# - https://github.com/eza-community/eza/issues/509
# - https://github.com/eza-community/eza/pull/575
#

# Python #######################################################################

export PYTHONSTARTUP="${HOME}/.config/python/startup.py"

# GitHub copilot ###############################################################

# TODO

# if command -v gh &> /dev/null
# then
#     eval "$(gh copilot alias -- zsh)"
# fi

# User specific aliases and functions ##########################################

# Don't use source unless necessary. When you use source, the script is executed
# in the current shell environment instead of in a new subshell. This means any
# commands in script.sh will directly affect the current shell, including
# options like set -e. This could terminate the terminal if an error occurs.

alias cat='bat'
alias ls='ls --color=auto'
alias eza='eza --long --binary --group --header --links --classify --time-style "+%a %_d %b %Y %R" --group-directories-first --git --git-repos-no-status'
alias ll='eza --all' # Second --all gives additional info
alias tree='eza --tree'
# alias ll='ls --all --human-readable -l --indicator-style=slash --hide-control-chars -v'
# alias tree='tree -a'
alias less='less --RAW-CONTROL-CHARS'
alias dd='dd status=progress'
alias df='df --print-type --human-readable --exclude-type tmpfs --exclude-type devtmpfs --local'
alias showdisks='ls -l /dev/disk/by-id'
alias pubip='curl icanhazip.com'
alias update='~/.dotfiles/scripts/update.sh'
alias yt-dlp='yt-dlp --write-subs'
alias temps='watch -n 1 sensors'
alias ping='ping -t 88' # People seeing my pings won't be able to guess my OS
alias hexyl='hexyl --no-squeezing --character-table ascii'
alias sshnk='ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"'
alias history='history -t "%F %T"'
alias vi='nvim'
alias vim='nvim'

mkcd() {
    mkdir -p "${1}" && cd "${1}"
}

up() {
    local count=${1:-1}
    local up_path=""
    for ((i=0; i<count; i++))
    do
        up_path="../${up_path}"
    done
    cd "${up_path}"
}
alias ..='up'

# ZFS plugins ##################################################################

# https://github.com/zsh-users/zsh-autosuggestions
# sudo dnf install zsh-autosuggestions
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# https://github.com/zsh-users/zsh-syntax-highlighting
# sudo dnf install zsh-syntax-highlighting

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main regexp)

# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[arg0]="fg=${CONSTANT_NUMERIC}"
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]="fg=${ESCAPED_CHAR}"
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]="fg=${ESCAPED_CHAR}"
ZSH_HIGHLIGHT_STYLES[command-substitution-delimiter]="fg=${CONTROL_FLOW}"
ZSH_HIGHLIGHT_STYLES[comment]="fg=${COMMENT}"
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]="fg=${KEYWORD_OR_VARIABLE}"
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]="fg=${STRING}"
ZSH_HIGHLIGHT_STYLES[globbing]="fg=${KEYWORD_OR_VARIABLE}"
ZSH_HIGHLIGHT_STYLES[path]="fg=${KEYWORD_OR_VARIABLE}"
ZSH_HIGHLIGHT_STYLES[path_prefix]="fg=${KEYWORD_OR_VARIABLE}"
ZSH_HIGHLIGHT_STYLES[process-substitution-delimiter]="fg=${CONTROL_FLOW}"
ZSH_HIGHLIGHT_STYLES[reserved-word]="fg=${CONTROL_FLOW}"
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]="fg=${STRING}"
ZSH_HIGHLIGHT_STYLES[unknown-token]="fg=${ERROR}"

# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/regexp.md
typeset -A ZSH_HIGHLIGHT_REGEXP
ZSH_HIGHLIGHT_REGEXP+=('\$([[:alnum:]_]+|\{[[:alnum:]_]+\})' "fg=${KEYWORD_OR_VARIABLE}") # Unquoted dollar variables (e.g. `$foo`, `${foo}` not in double quotes)

# **Must be last!** https://github.com/zsh-users/zsh-syntax-highlighting?tab=readme-ov-file#why-must-zsh-syntax-highlightingzsh-be-sourced-at-the-end-of-the-zshrc-file
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
