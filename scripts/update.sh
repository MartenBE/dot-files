#!/usr/bin/env bash

set -e

echo "=== Dot files repo ======================================================="
cd "${HOME}/.dotfiles" || exit 1
git pull
stow --dir "${HOME}/.dotfiles" --target "${HOME}" --verbose .
echo

echo "=== DNF =================================================================="
dnf_excludes=(--exclude=kernel* --exclude=zfs*)
echo "Excludes: ${dnf_excludes[*]}"
sudo dnf upgrade --refresh "${dnf_excludes[@]}" --assumeyes
echo

echo "=== Flatpak =============================================================="
if command -v flatpak &> /dev/null
then
    flatpak update --assumeyes
    flatpak remove --unused --assumeyes
else
    echo "Flatpak is not installed"
fi
echo

echo "=== Python ==============================================================="
if command -v uv &> /dev/null
then
    uv tool upgrade --all
    echo
else
    echo "Uv is not installed"
fi
echo

echo "=== Node.js =============================================================="
if command -v npm &> /dev/null
then
    outdated_packages=$(npm outdated --global --parseable --depth=0 | cut -d: -f4)

    if [ -z "$outdated_packages" ]
    then
        echo "Nothing to upgrade"
    else
        for package in $outdated_packages
        do
            sudo npm install --global "$package"
            echo
            echo "$package has been updated."
        done
    fi
    echo
else
    echo "Npm is not installed"
fi
echo

echo "=== Neovim ==============================================================="
if command -v nvim &> /dev/null
then
    nvim --headless "+Lazy! sync" +qa | grep --fixed-strings 'Finished task checkout' | sort
    echo "Updated plugins"
    echo
    nvim --headless -c "TSUpdateSync" -c 'qa'
    echo # Newline necessary
    echo "Updated treesitter language parsers"
    echo
    nvim --headless -c "MasonUpdate" -c 'qa'
    echo # Newline necessary
    echo "Updated mason registries"
    echo
    nvim --headless -c "MasonToolsUpdateSync" -c 'qa'
    echo # Newline necessary
    echo "Updated mason tools"
    echo
else
    echo "Neovim is not installed"
fi
echo

echo "=== Tldr ================================================================="
if command -v tldr &> /dev/null
then
    tldr --update
else
    echo "Tldr is not installed"
fi
echo

echo "Done ..."
