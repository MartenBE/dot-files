#!/usr/bin/env bash

set -e

# RPM Fusion

sudo dnf --assumeyes install \
    "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
    "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"

# DNF packages

sudo dnf --assumeyes install \
    bat \
    black \
    btop \
    cascadia-code-fonts \
    clang \
    clang-tools-extra \
    cmatrix \
    duf \
    eza \
    fastfetch \
    fd-find \
    filezilla \
    fira-code-fonts \
    firefox \
    foliate \
    fzf \
    gcc \
    g++ \
    git \
    git-lfs \
    gromit-mpx \
    hexyl \
    htop \
    httpie \
    iotop \
    java-latest-openjdk \
    java-latest-openjdk-devel \
    jetbrains-mono-fonts \
    jq \
    kamoso \
    keyd \
    keepassxc \
    lm_sensors \
    lnav \
    mangohud \
    meld \
    ncdu \
    neovim \
    nethogs \
    nextcloud-client-dolphin \
    nmap \
    nodejs \
    openrgb \
    picocom \
    pre-commit \
    python3-neovim \
    python3-pip \
    ripgrep \
    shellcheck \
    speedtest-cli \
    steam \
    stow \
    tcpdump \
    tealdeer \
    thefuck \
    tmux \
    uv \
    VirtualBox \
    wine \
    wireshark \
    yamllint \
    yacreader \
    yt-dlp \
    zint-qt \
    zoxide \
    zsh \
    zsh-autosuggestions \
    zsh-syntax-highlighting \

# Node packages

sudo npm install --global prettier
sudo npm install --global prettier-plugin-java
sudo npm install --global markdownlint-cli2

# Pip packages

uv tool install ski-lint
uv tool install subliminal
uv tool install ffsubsync
uv tool install markitdown

# Flatpaks

sudo dnf --assumeyes install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub \
    com.discordapp.Discord \
    com.github.iwalton3.jellyfin-media-player \
    com.spotify.Client \
    org.videolan.VLC \
    im.riot.Riot \
    io.missioncenter.MissionCenter \
    org.videolan.VLC \

# Copr

sudo dnf --assumeyes copr enable zawertun/hack-fonts
sudo dnf --assumeyes install hack-fonts

sudo dnf copr --assumeyes enable atim/starship
sudo dnf --assumeyes install starship

sudo dnf copr --assumeyes enable wezfurlong/wezterm-nightly
sudo dnf --assumeyes install wezterm

sudo dnf copr --assumeyes enable varlad/zellij
sudo dnf --assumeyes install zellij

# Custom repositories

if [ ! -f "/etc/yum.repos.d/vscode.repo" ]
then
    sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
    sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    sudo dnf --assumeyes install code
fi

if [ ! -f "/etc/yum.repos.d/vivaldi-fedora.repo" ]
then
    sudo dnf --assumeyes config-manager addrepo --from-repofile=https://repo.vivaldi.com/archive/vivaldi-fedora.repo
    sudo dnf --assumeyes install vivaldi-stable
fi

if [ ! -f "/etc/yum.repos.d/docker-ce.repo" ]
then
    sudo dnf --assumeyes remove \
        docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-selinux \
        docker-engine-selinux \
        docker-engine
    sudo dnf --assumeyes config-manager addrepo --from-repofile=https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf --assumeyes install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo systemctl enable --now docker
fi

if [ ! -f "/etc/yum.repos.d/shells:zsh-users:zsh-completions.repo" ]
then
    sudo dnf --assumeyes config-manager addrepo --from-repofile=https://download.opensuse.org/repositories/shells:zsh-users:zsh-completions/Fedora_Rawhide/shells:zsh-users:zsh-completions.repo
    sudo dnf --assumeyes install zsh-completions
fi

# Vagrant, terraform, ...
if [ ! -f "/etc/yum.repos.d/hashicorp.repo" ]
then
    sudo sudo dnf --assumeyes config-manager addrepo --from-repofile=https://rpm.releases.hashicorp.com/fedora/hashicorp.repo
fi

if ! rpm -q onlyoffice-desktopeditors > /dev/null
then
    sudo dnf --assumeyes install https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors.x86_64.rpm
    sudo dnf --assumeyes install onlyoffice-desktopeditors
fi

if [ ! -f "/etc/yum.repos.d/gh-cli.repo" ]
then
    sudo dnf --assumeyes config-manager addrepo --from-repofile=https://cli.github.com/packages/rpm/gh-cli.repo
    sudo dnf --assumeyes install gh --repo gh-cli
fi

# Bugfixes

# https://discussion.fedoraproject.org/t/after-upgrading-to-kernel-6-12-virtualbox-fails-to-start/139896/9
sudo grubby --update-kernel=ALL --args="kvm.enable_virt_at_load=0"

# Config

sudo chsh -s "$(which zsh)" "${USER}"

echo "All done ..."
