-- Autocmd adds to the list of autocommands regardless of whether they are
-- already present. When your .vimrc file is sourced twice, the autocommands will
-- appear twice. To avoid this, define your autocommands in a group, so that you
-- can easily clear them.

local DeleteTrailingWhitespace = vim.api.nvim_create_augroup("DeleteTrailingWhitespace", { clear = true })
vim.api.nvim_create_autocmd(
"BufWritePre",
{
    command = ":%s/\\s\\+$//e", -- TODO: use <cmd> and <cr>
    group = DeleteTrailingWhitespace,
}
)

-- Triger `autoread` when files changes on disk
-- https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
-- https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
vim.api.nvim_create_autocmd({"FocusGained", "BufEnter", "CursorHold", "CursorHoldI"}, {
    pattern = "*",
    command = "if mode() !~ '\v(c|r.?|!|t)' && getcmdwintype() == '' | checktime | endif",
})

-- Notification after file change
-- https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
vim.api.nvim_create_autocmd({"FileChangedShellPost"}, {
    pattern = "*",
    command = "echohl WarningMsg | echo 'File changed on disk at ' .. strftime('%Y-%m-%d %H:%M:%S') .. '. Buffer reloaded.' | echohl None",
})

-- Highlight when yanking (copying) text
vim.api.nvim_create_autocmd('TextYankPost', {
    desc = 'Highlight when yanking (copying) text',
    group = vim.api.nvim_create_augroup('kickstart-highlight-yank', { clear = true }),
    callback = function()
        vim.highlight.on_yank()
    end,
})

-- Enable spell checking for text, markdown, and tex files
vim.api.nvim_create_autocmd("FileType", {
    pattern = {"text", "markdown", "tex"},
    callback = function()
        vim.opt_local.spell = true
        vim.opt_local.spelllang = "en_us,nl,programming"
    end
})
