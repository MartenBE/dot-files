vim.opt.background = "dark" -- Set dark theme
vim.opt.mouse = nil -- Disable mouse
vim.opt.number = true -- Enable line numbers
vim.opt.cursorline = true -- Highlight cursorline
vim.opt.colorcolumn = "81,121" -- Show col for max line length
vim.opt.signcolumn = "auto:9" -- Show the sign column
vim.opt.syntax = "on" -- Allow syntax highlighting
vim.opt.termguicolors = true -- If term supports ui color then enable
vim.opt.wrap = false -- Don't wrap lines
vim.opt.breakindent = true -- Every wrapped line will continue visually indented (same amount of space as the beginning of that line)
vim.opt.showbreak = "↪" -- Symbol to show on the next line when text is wrapped
vim.opt.swapfile = false -- Don"t use swapfiles
vim.opt.fileencoding = "utf8" -- File encoding to use
local amount_spaces_indentation = 4
vim.opt.tabstop = amount_spaces_indentation -- Tell vim how many columns a tab counts for
vim.opt.softtabstop = amount_spaces_indentation -- control how many columns vim uses when you hit Tab in insert mode
vim.opt.shiftwidth = amount_spaces_indentation -- Control how many columns text is indented with the reindent operations
vim.opt.expandtab = true -- Hitting Tab in insert mode will produce the appropriate number of spaces
vim.opt.list = false -- Do not show whitespace characters by default
vim.opt.listchars = {
	tab = "» ",
	space = "•",
	trail = "•",
	eol = "¶",
	nbsp = "␣",
	extends = "⟩",
	precedes = "⟨",
} -- Symbols to show instead of whitespace characters
vim.opt.ignorecase = true -- Ignore case by default
vim.opt.smartcase = true -- Make search case sensitive only if it contains uppercase letters
vim.opt.splitbelow = true -- Open new split panes to bottom
vim.opt.splitright = true -- Open new split panes to right
vim.schedule(function()
	vim.opt.clipboard = "unnamedplus" -- Sync clipboard between OS and Neovim
end) --  Schedule the setting after `UiEnter` because it can increase startup-time.
vim.opt.timeoutlen = 500 -- Don"t wait a full second for mappings to be parsed (notices with Comments.nvim)
vim.opt.wildignorecase = true -- Ignore case when autocompleting vim commands
vim.opt.wildmode = "longest:full,full" -- Find longest common subsequence for vim command autocompletion and show list, autofill on second Tab
vim.o.completeopt = "menuone,noselect" -- Set completeopt to have a better completion experience
-- vim.opt.relativenumber = true -- Enable relative linenumbers
-- vim.opt.statuscolumn = "%s %l %r " -- Display signs, linenumbers and relative linenumbers in the gutter
vim.opt.autoread = true -- Automatically reload files when changed outside of vim
vim.opt.inccommand = "split" -- Show live preview of substitution

local signs = {
  { name = "DiagnosticSignError", icon = "", severity = "ERROR" },
  { name = "DiagnosticSignWarn", icon = "", severity = "WARN" },
  { name = "DiagnosticSignHint", icon = "󰌵", severity = "HINT" },
  { name = "DiagnosticSignInfo", icon = "", severity = "INFO" },
}

-- Signs for gutter and create lookup table for virtual text
local severity_lookup = {}
for _, sign in ipairs(signs) do
  vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.icon, numhl = "" })
  severity_lookup[sign.severity] = sign.icon
end

-- Signs for virtual text
vim.diagnostic.config {
  virtual_text = {
    prefix = function(diagnostic)
      local severity = vim.diagnostic.severity[diagnostic.severity]
      return severity_lookup[severity]
    end,
  },
}

