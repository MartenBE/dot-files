function draw_horizontal_rule(char, endcolumn)
    local row, column = unpack(vim.api.nvim_win_get_cursor(0))
    local line = vim.api.nvim_get_current_line()

    -- Determine char to draw the line with
    if not char then
        if not line or string.len(line) == 0 then
            vim.api.nvim_notify("ERROR: No char", vim.log.levels.ERROR, {})
            return
        end

        char = string.sub(line, -1)
    end

    -- Determine the end to where to draw the line
    if not endcolumn then
        local colorcolumn = vim.opt.colorcolumn:get()
        if not next(colorcolumn) then
            vim.api.nvim_notify("ERROR: No endcolumn and no colorcolumn", vim.log.levels.ERROR, {})
            return
        end

        local i = 1
        while i <= #colorcolumn and column > tonumber(colorcolumn[i]) do
            vim.api.nvim_notify(colorcolumn[i], vim.log.levels.INFO, {})
            i = i + 1
        end

        if i > #colorcolumn then
            vim.api.nvim_notify("ERROR: Current column is greater than all colorcolumn values", vim.log.levels.ERROR, {})
            return
        end

        endcolumn = tonumber(colorcolumn[i])
        vim.api.nvim_notify("INFO: Selected " .. endcolumn .. " from colorcolumn values as endcolumn", vim.log.levels.INFO, {})
    end

    amount_char = (endcolumn - string.len(line))
    vim.api.nvim_set_current_line(line .. string.rep(char, amount_char))
end

function ToggleLightDarkTheme()
    if vim.opt.background:get() == "dark" then
        require("vscode").load("light")
        vim.opt.background = "light"
        vim.notify("Switched to light theme", vim.log.levels.INFO)
    else
        require("vscode").load("dark")
        vim.opt.background = "dark"
        vim.notify("Switched to dark theme", vim.log.levels.INFO)
    end
end
