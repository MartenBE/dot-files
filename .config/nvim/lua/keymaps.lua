-- n: works in normal mode.
-- i: works in insert mode.
-- v: works in visual and select modes.
-- x: works in visual mode.
-- s: works in select mode.
-- c: works in command-line mode.
-- o: works in operator pending mode.
-- silent: prevents displaying the sequence of keys executed

-- Don't set insert mode keybindings with leader as this will introduce delays when typing, especially if the leader key is space.
-- If this happens, check the offending keymap with `:verbose imap <leader>`
-- https://www.reddit.com/r/neovim/comments/y12zjk/when_setting_space_as_leader_then_there_is_a/

vim.keymap.set("n", "<esc>", "<cmd>nohlsearch<cr>", { desc = "Removes search highlight for current search" })
vim.keymap.set("n", "<leader><tab>", "<cmd>bn<cr>", { desc = "Switch to next buffer" })
vim.keymap.set("n", "<leader>cw", "g<C-g>", { desc = "Count words in buffer" })
vim.keymap.set("n", "<leader>q", "q", { desc = "Remap recording" })
vim.keymap.set("n", "<leader>ts", "<cmd>set list!<cr>", { desc = "Toggle view whitespace" })
vim.keymap.set("n", "<leader>tw", "<cmd>set wrap!<cr>", { desc = "Toggle word wrap" })
vim.keymap.set("n", "q", "<Nop>", { desc = "Remove original recording map" })

-- -- Indentation
-- ---- Indent
-- vim.keymap.set("i", "<S-Tab>", "<C-d>")
-- vim.keymap.set("n", "<S-Tab>", "<<")
-- vim.keymap.set("v", "<S-Tab>", "<gv")
-- ---- Unindent
-- vim.keymap.set("i", "<Tab>", "<C-t>")
-- vim.keymap.set("n", "<Tab>", ">>")
-- vim.keymap.set("v", "<Tab>", ">gv")

-- Custom functions
vim.keymap.set("n", "<leader>dr", "<cmd>lua draw_horizontal_rule()<cr>", { desc = "Draw horizontal rule" })
vim.keymap.set("n", "<leader>th", "<cmd>lua ToggleLightDarkTheme()<cr>", { desc = "Toggle light/dark theme" })

-- Plugin keymaps are specified in the plugin configuration files with the `key` key.
