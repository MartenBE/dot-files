return {
    "stevearc/oil.nvim",
    opts = {
        view_options = {
            show_hidden = true,
        },
    },
    dependencies = { "nvim-tree/nvim-web-devicons" },
    lazy = false, -- Otherwise won't work with custom keys option below? https://github.com/stevearc/oil.nvim/issues/409
    keys = {
        { "-", "<cmd>Oil<cr>", mode = "n" },
        { "<leader>-", "<cmd>Oil --float<cr>", mode = "n"  },
    },
}

