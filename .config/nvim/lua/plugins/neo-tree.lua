return {
	"nvim-neo-tree/neo-tree.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
		"MunifTanjim/nui.nvim",
		-- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
	},
	opts = {
		hide_dotfiles = false,
		hide_gitignored = false,
		hide_hidden = false,
		filesystem = {
			follow_current_file = {
				enable = true,
			},
		},
	},
	keys = {
		{ "<leader>tr", "<cmd>Neotree toggle reveal<cr>", mode = "n", desc = "Toggle file tree" },
	},
}
