return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function() -- `opts = {}` doesn't work, see https://github.com/folke/lazy.nvim/issues/1917
        require("nvim-treesitter.configs").setup({
            ensure_installed = "all",
            auto_install = false,

            highlight = {
                enable = true
            },
            indent = {
                enable = true
            },
        })
    end,
}