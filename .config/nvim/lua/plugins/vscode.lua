return {
	"Mofiqul/vscode.nvim",
	config = function()
		-- Generated with ChatGPT
		-- TODO: Understand and see if correct
		local function blend(color, bg, alpha)
			local function hex_to_rgb(hex)
				return tonumber(hex:sub(2, 3), 16), tonumber(hex:sub(4, 5), 16), tonumber(hex:sub(6, 7), 16)
			end

			local function rgb_to_hex(r, g, b)
				return string.format("#%02X%02X%02X", r, g, b)
			end

			local r1, g1, b1 = hex_to_rgb(color)
			local r2, g2, b2 = hex_to_rgb(bg)
			local r = math.floor(r1 * alpha + r2 * (1 - alpha))
			local g = math.floor(g1 * alpha + g2 * (1 - alpha))
			local b = math.floor(b1 * alpha + b2 * (1 - alpha))

			return rgb_to_hex(r, g, b)
		end

		local c = require("vscode.colors").get_colors()
		require("vscode").setup({
			italic_comments = true,
			underline_links = true,
			group_overrides = {
				CopilotSuggestion = { fg = "#555555", ctermfg = 8 },

				DiagnosticVirtualTextOk = { fg = blend(c.vscBlueGreen, c.vscBack, 0.5) }, -- Dimmed vscBlueGreen
				DiagnosticVirtualTextError = { fg = blend(c.vscRed, c.vscBack, 0.5) }, -- Dimmed vscRed
				DiagnosticVirtualTextWarn = { fg = blend(c.vscYellow, c.vscBack, 0.5) }, -- Dimmed vscYellow
				DiagnosticVirtualTextInfo = { fg = blend(c.vscBlue, c.vscBack, 0.5) }, -- Dimmed vscBlue
				DiagnosticVirtualTextHint = { fg = blend(c.vscBlue, c.vscBack, 0.5) }, -- Dimmed vscBlue
			},
		})
		require("vscode").load() -- why?
	end,

	-- If colorscheme plugins are lazy loaded need to be higher prioritized (https://lazy.folke.io/spec/lazy_loading)
	lazy = false,
	priority = 1000,
}
