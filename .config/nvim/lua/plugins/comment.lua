return {
	"numToStr/Comment.nvim",
	dependencies = {
		{
			-- Detects difference between TS,HTML, ... in Vue files and other mixed files
			"JoosepAlviste/nvim-ts-context-commentstring",
			opts = {
				enable_autocmd = false,
			},
		},
	},
	opts = {
		pre_hook = function()
			require("ts_context_commentstring.integrations.comment_nvim").create_pre_hook()
		end,
	},
}
