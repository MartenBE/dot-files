return {
	"ibhagwan/fzf-lua",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {
		-- Necessary to load the plugin: https://lazy.folke.io/developers
	},
	keys = {
		{ "<leader>sb", "<cmd>FzfLua blines<cr>", mode = "n", desc = "Search in current buffer" },
		{ "<leader>sc", "<cmd>FzfLua files cwd=~/.config/nvim<cr>", mode = "n", desc = "Search files in config path" },
		{ "<leader>sf", "<cmd>FzfLua files<cr>", mode = "n", desc = "Search files" },
		{ "<leader>sh", "<cmd>FzfLua oldfiles<cr>", mode = "n", desc = "Search file history" },
		{ "<leader>sk", "<cmd>FzfLua keymaps<cr>", mode = "n", desc = "Search keymaps" },
		{ "<leader>sl", "<cmd>FzfLua lines<cr>", mode = "n", desc = "Search in open buffer" },
		{ "<leader>sp", "<cmd>FzfLua grep_project<cr>", mode = "n", desc = "Search in project" },
		{ "<leader>ss", "<cmd>FzfLua<cr>", mode = "n", desc = "Catch all for fzf" },
	},
}

-- https://github.com/ibhagwan/fzf-lua/wiki#whats-the-difference-between-grep-and-live_grep
-- https://github.com/ibhagwan/fzf-lua/discussions/1139
-- grep: Execute `grep` once at first, then FZF on the results (fuzzy)
-- live_grep: Execute `grep` each time the query changes (regexp)
