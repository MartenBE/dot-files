return {
    "kylechui/nvim-surround",
    version = "*",
    event = "VeryLazy",
    opts = {
        -- Necessary to load the plugin: https://lazy.folke.io/developers
    },
}
