return {
	"Wansmer/treesj",
	keys = { "<space>m", "<space>j", "<space>s" },
	dependencies = { "nvim-treesitter/nvim-treesitter" }, -- if you install parsers with `nvim-treesitter`
	opts = {
		use_default_keymaps = false,
		max_join_length = 500,
	},
	keys = {
		{ "gj", "<cmd>TSJToggle<cr>", desc = "Treesitter split/join" },
	},
}
