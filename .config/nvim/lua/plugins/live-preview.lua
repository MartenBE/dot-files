return {
	"brianhuster/live-preview.nvim",
	dependencies = {
		"ibhagwan/fzf-lua",
	},
	keys = {
		{ "<leader>ms", "<cmd>LivePreview start<cr>", mode = "n", desc = "Start live preview" },
		{ "<leader>mc", "<cmd>LivePreview close<cr>", mode = "n", desc = "Close live preview" },
	},
}
