return {
	"CopilotC-Nvim/CopilotChat.nvim", -- for chat
	dependencies = {
		{
			"zbirenbaum/copilot.lua", -- for autocompletion
			cmd = "Copilot",
			event = "InsertEnter",
			opts = {
				suggestion = {
					auto_trigger = true,
				},
				-- filetypes = {
				-- 	markdown = true,
				-- 	yaml = true,
				-- },
			},
		},
		{ "nvim-lua/plenary.nvim", branch = "master" }, -- for curl, log and async functions
	},
	build = "make tiktoken", -- Only on MacOS or Linux
	opts = {
		-- See Configuration section for options
	},
	-- See Commands section for default commands if you want to lazy load on them
	keys = {
		{ "<leader>ta", "<cmd>CopilotChatToggle<cr>", mode = { "n", "v" }, desc = "Toggle copilotChat" },
		-- Explain: Write an explanation for the selected code as paragraphs of text
		{ "<leader>ae", "<cmd>CopilotChatExplain<cr>", mode = "v", desc = "CopilotChat: explain code" },
		-- Review: Review the selected code
		{ "<leader>ar", "<cmd>CopilotChatReview<cr>", mode = "v", desc = "CopilotChat: review code" },
		-- Fix: There is a problem in this code. Rewrite the code to show it with the bug fixed
		{ "<leader>af", "<cmd>CopilotChatFix<cr>", mode = "v", desc = "CopilotChat: fix code" },
		-- Optimize: Optimize the selected code to improve performance and readability
		{ "<leader>ao", "<cmd>CopilotChatOptimize<cr>", mode = "v", desc = "CopilotChat: optimize" },
		-- Docs: Please add documentation comments to the selected code
		-- Tests: Please generate tests for my code
		-- Commit: Write commit message for the change with commitizen convention
	},
}
