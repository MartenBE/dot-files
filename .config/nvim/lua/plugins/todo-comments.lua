return {
    "folke/todo-comments.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = {
        -- Necessary to load the plugin: https://lazy.folke.io/developers
    },
}
