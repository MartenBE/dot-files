return {
	"sindrets/diffview.nvim",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	config = function()
		vim.opt.fillchars:append({ diff = "╱" })

		require("diffview").setup({
			enhanced_diff_hl = true,
		})

		vim.keymap.set("n", "<leader>do", "<cmd>DiffviewOpen<cr>", { desc = "Open diffview" })
		vim.keymap.set("n", "<leader>dc", "<cmd>DiffviewClose<cr>", { desc = "Close diffview" })
	end,
}
