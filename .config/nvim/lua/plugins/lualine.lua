return {
    "nvim-lualine/lualine.nvim",
    dependencies = {
        "nvim-tree/nvim-web-devicons",
        "Mofiqul/vscode.nvim",
        "AndreM222/copilot-lualine",
    },
    opts = {
        options= {
            theme = "vscode",
        },
        sections = {
            lualine_a = {
                { "mode" },
            },
            lualine_b = {
                { "branch" },
                { "diff" },
                { "diagnostics" },
            },
            lualine_c = {
                {
                    "filename",
                    newfile_status = true,
                    symbols = {
                        modified = "●",
                        readonly = "🔒",
                        unnamed = "[No Name]",
                        newfile = "[New]",
                    },
                },
            },
            lualine_x = {
                { "copilot" },
                {
                    "encoding",
                    show_bomb = true,
                },
                { "fileformat" },
                { "filetype" },
            },
            lualine_y = {
                { "progress" },
            },
            lualine_z = {
                { "location" },
            },
        },
    },
}