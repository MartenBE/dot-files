-- Must come **after** the colorscheme!
-- https://github.com/lukas-reineke/indent-blankline.nvim/issues/725
-- https://github.com/Mofiqul/vscode.nvim/issues/152

return {
    "lukas-reineke/indent-blankline.nvim",
    dependencies = "Mofiqul/vscode.nvim",
    main = "ibl",
    opts = {
        indent = {
            char = "│",
            highlight = { "IndentBlanklineChar" }
        },
        scope = {
            highlight = { "IndentBlanklineContextChar" }
        },
    },
}