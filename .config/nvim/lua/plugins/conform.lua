return {
    "stevearc/conform.nvim",
    event = { "BufWritePre" },
    cmd = { "ConformInfo" },
    keys = {
        {
            "<leader>f",
            function()
                require("conform").format({ async = true })
            end,
            mode = "",
            desc = "Format buffer",
        },
    },
    opts = {
        default_format_opts = {
            lsp_format = "fallback",
        }, 
        formatters_by_ft = {
            javascript = { "prettier" },
            typescript = { "prettier" },
            css = { "prettier" },
            html = { "prettier" },
            json = { "prettier" },
            lua = { "stylua" },
            yaml = { "prettier" },
            markdown = { "prettier" },
            python = { "isort", "black" },
            ["_"] = { "trim_whitespace", "trim_newlines", "indent_buffer" },
        },
        formatters = {
            -- Custom formatter to auto indent buffer.
            -- https://github.com/stevearc/conform.nvim/issues/255
            indent_buffer = {
                format = function(_, ctx, _, callback)
                    -- save cursor position in ` mark -> indent -> restore cursor position
                    if ctx.range == nil then
                        -- no range, use whole buffer
                        vim.cmd.normal({ "m`gg=G``", bang = true })
                    else
                        -- use range
                        vim.cmd.normal({ "m`=``", bang = true })
                    end
                    callback()
                end,
            },
        },
    },
}