return {
	"lewis6991/gitsigns.nvim",
	opts = {},
	keys = {
		{ "gbb", ":Gitsigns blame<cr>", { mode = "n", desc = "Git blame" } },
		{ "gbl", ":Gitsigns blame_line<cr>", { mode = "n", desc = "Git blame (line)" } },
	},
}
