return { 
    "mfussenegger/nvim-dap",
    version="*",
    dependencies = {
        "rcarriga/nvim-dap-ui",
        "theHamsta/nvim-dap-virtual-text",
        "mfussenegger/nvim-dap-python", -- Do this for each project: https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#python
        "nvim-neotest/nvim-nio", -- dependency for nvim-dap-ui
    },
    config = function()
        local dap = require("dap")

        vim.keymap.set("n", "<leader>b", dap.toggle_breakpoint)
        vim.keymap.set("n", "<F5>", dap.continue)
        vim.keymap.set("n", "<F6>", dap.step_into)
        vim.keymap.set("n", "<F7>", dap.step_over)
        vim.keymap.set("n", "<F8>", dap.step_out)
        vim.keymap.set("n", "<F9>", dap.step_back)
        vim.keymap.set("n", "<F12>", dap.restart)
        
        local dapui = require("dapui")
        dapui.setup()

        -- See :h dap-launch.json (section "SIGNS CONFIGURATION").
        vim.fn.sign_define("DapBreakpoint", {text="🔴", texthl="", linehl="", numhl=""})
        vim.fn.sign_define("DapStopped", {text="🡆", texthl="", linehl="", numhl=""})

        dap.listeners.before.attach.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.launch.dapui_config = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated.dapui_config = function()
            dapui.close()
        end
        dap.listeners.before.event_exited.dapui_config = function()
            dapui.close()
        end

        require("nvim-dap-virtual-text").setup()

        require("dap-python").setup("python3")
    end,
}
