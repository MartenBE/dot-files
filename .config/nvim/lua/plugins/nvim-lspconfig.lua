return {
	-- Main LSP Configuration by https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua
	"neovim/nvim-lspconfig",
	dependencies = {
		{
			"williamboman/mason.nvim", -- Automatically install LSPs and related tools to stdpath for Neovim
			config = true,
		}, -- NOTE: Must be loaded before dependants
		"williamboman/mason-lspconfig.nvim",
		"WhoIsSethDaniel/mason-tool-installer.nvim",
		"nvim-java/nvim-java",
		{
			"j-hui/fidget.nvim", -- Useful status updates for LSP
			opts = {
				-- Necessary to load the plugin: https://lazy.folke.io/developers
			},
		},
	},
	config = function()
		-- Brief aside: **What is LSP?**
		--
		-- LSP is an initialism you've probably heard, but might not understand what it is.
		--
		-- LSP stands for Language Server Protocol. It's a protocol that helps editors
		-- and language tooling communicate in a standardized fashion.
		--
		-- In general, you have a "server"which is some tool built to understand a particular
		-- language (such as `gopls`, `lua_ls`, `rust_analyzer`, etc.). These Language Servers
		-- (sometimes called LSP servers, but that's kind of like ATM Machine) are standalone
		-- processes that communicate with some "client"- in this case, Neovim!
		--
		-- LSP provides Neovim with features like:
		--  - Go to definition
		--  - Find references
		--  - Autocompletion
		--  - Symbol Search
		--  - and more!
		--
		-- Thus, Language Servers are external tools that must be installed separately from
		-- Neovim. This is where `mason` and related plugins come into play.
		--
		-- If you're wondering about lsp vs treesitter, you can check out the wonderfully
		-- and elegantly composed help section, `:help lsp-vs-treesitter`

		--  This function gets run when an LSP attaches to a particular buffer.
		--    That is to say, every time a new file is opened that is associated with
		--    an lsp (for example, opening `main.rs` is associated with `rust_analyzer`) this
		--    function will be executed to configure the current buffer
		vim.api.nvim_create_autocmd("LspAttach", {
			group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
			callback = function(event)
				fzf_lua = require("fzf-lua")

				-- Jump to the definition of the word under your cursor
				vim.keymap.set("n", "gd", fzf_lua.lsp_definitions, { desc = "[G]oto [D]efinition" })

				-- Goto Declaration (this is distinct from Goto Definition)
				vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { desc = "[G]oto [D]eclaration" })

				-- Find references for the word under your cursor
				vim.keymap.set("n", "gr", fzf_lua.lsp_references, { desc = "[G]oto [R]eferences" })

				-- Jump to the implementation of the word under your cursor
				vim.keymap.set("n", "gI", fzf_lua.lsp_implementations, { desc = "[G]oto [I]mplementation" })

				-- Jump to the type definition of the word under your cursor
				vim.keymap.set("n", "<leader>D", fzf_lua.lsp_typedefs, { desc = "Type [D]efinition" })

				-- Rename the variable under the cursor
				vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { desc = "[R]e[n]ame" })

				-- Execute a code action
				vim.keymap.set({ "n", "x" }, "<leader>ca", vim.lsp.buf.code_action, { desc = "[C]ode [A]ction" })
			end,
		})

		-- LSP servers and clients are able to communicate to each other what features they support. By default, Neovim doesn"t support everything that is in the LSP specification. When you add nvim-cmp, luasnip, etc. Neovim now has *more* capabilities. So, we create new capabilities with nvim cmp, and then broadcast that to the servers.
		local capabilities = vim.lsp.protocol.make_client_capabilities()
		capabilities = vim.tbl_deep_extend("force", capabilities, require("blink.cmp").get_lsp_capabilities())

		-- Enable the following language servers
		--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
		--
		--  Add any additional override configuration in the following tables. Available keys are:
		--  - cmd (table): Override the default command used to start the server
		--  - filetypes (table): Override the default list of associated filetypes for the server
		--  - capabilities (table): Override fields in capabilities. Can be used to disable certain LSP features.
		--  - settings (table): Override the default settings passed when initializing the server.
		--        For example, to see the options for `lua_ls`, you could go to: https://luals.github.io/wiki/settings/
		local servers = {
			-- https://github.com/williamboman/mason-lspconfig.nvim
			ansiblels = {},
			bashls = {},
			clangd = {},
			cssls = {},
			docker_compose_language_service = {},
			gradle_ls = {},
			html = {},
			jinja_lsp = {},
			jsonls = {},
			lua_ls = {},
			pyright = {},
			rust_analyzer = {},
			terraformls = {},
			texlab = {},
			ts_ls = {
				-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md#vue-support
				init_options = {
					plugins = {
						{
							name = "@vue/typescript-plugin",
							location = vim.fn.stdpath("data")
								.. "/mason/packages/vue-language-server/node_modules/@vue/language-server/node_modules/@vue/typescript-plugin",
							languages = { "javascript", "typescript", "vue" },
						},
					},
				},
				filetypes = {
					"javascript",
					"typescript",
					"vue",
				},
			},
			volar = {},
			yamlls = {},
		}

		-- Ensure the servers and tools above are installed. To check the current status of installed tools and/or manually install other tools, you can run `:Mason`. You can press `g?` for help in this menu.
		require("mason").setup()

		-- You can add other tools here that you want Mason to install for you, so that they are available from within Neovim.
		local ensure_installed = vim.tbl_keys(servers or {})
		vim.list_extend(ensure_installed, {
			-- conform.nvim
			"black",
			"isort",
			"prettier",
			"stylua",
			-- nvim-lint.nvim
			"markdownlint-cli2",
			"shellcheck",
			"yamllint",
		})
		require("mason-tool-installer").setup({
			ensure_installed = ensure_installed,
		})

		require("mason-lspconfig").setup({
			handlers = {
				function(server_name)
					local server = servers[server_name] or {}
					-- This handles overriding only values explicitly passed by the server configuration above. Useful when disabling certain features of an LSP (for example, turning off formatting for ts_ls)
					server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
					require("lspconfig")[server_name].setup(server)
				end,
			},
			jdtls = function() -- nvim-java
				require("java").setup({
					-- Your custom jdtls settings goes here
				})

				require("lspconfig").jdtls.setup({
					-- Your custom nvim-java configuration goes here
				})
			end,
		})
	end,
}
