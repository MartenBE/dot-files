return {
	"Isrothy/neominimap.nvim",
	version = "*",
	enabled = true,
	keys = {
		{ "<leader>tm", "<cmd>Neominimap toggle<cr>", desc = "Toggle global minimap" },
	},
	init = function()
		vim.g.neominimap = {
			auto_enable = true,
			layout = "split",
			click = {
				enabled = true,
			},
			diagnostic = {
				mode = "icon",
			},
		}
	end,
}
