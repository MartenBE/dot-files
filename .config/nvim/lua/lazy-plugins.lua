require("lazy").setup({
	require("plugins/better-escape"), -- Better escape using `jj`
	require("plugins/bufferline"), -- Status line for buffers
	require("plugins/blink"), -- Autocompletion menu
	require("plugins/coerce"), -- Switch between camelCase, PascalCase, snake_case, kebab-case, ...
	require("plugins/comment"), -- Better support for comments (e.g. block comments)
	require("plugins/conform"), -- Formatting
	require("plugins/copilot"), -- Autocompletion and chat using GitHub Copilot
    require("plugins/csvview"), -- Edit CSV  files more visually
	require("plugins/diffview"), -- Show git diffs
	require("plugins/editorconfig"), -- EditorConfig support
	require("plugins/fidget"), -- Show information about LSP, ... in the lower right corner
	require("plugins/fzf-lua"), -- Fuzzy finder
	require("plugins/gitsigns"), -- Show git changes in the gutter
	require("plugins/hardtime"), -- Force good vim habbits
	require("plugins/indent-blankline"), -- Show indent lines
	require("plugins/live-preview"), -- Open MarkDown, HTML, SVG, ... preview like VSCode
	require("plugins/lorem"), -- Insert lorem ipsum text
	require("plugins/lualine"), -- Status line
	require("plugins/neo-tree"), -- File explorer using tree layout
	require("plugins/neominimap"), -- Minimap
	require("plugins/nvim-autopairs"), -- Automatically add closing brackets, quotes, ...
	require("plugins/nvim-dap"), -- Debug Adapter Protocol configuration
	require("plugins/nvim-highlight-colors"), -- Highlight colors in hex, rgb, ...
	require("plugins/nvim-lint"), -- Linting
	require("plugins/nvim-lspconfig"), -- Language Server Protocol configuration
	require("plugins/nvim-numbertoggle"), -- Relative line numbers based on normal/insert/... mode
	require("plugins/nvim-surround"), -- Surround text with quotes, brackets, ...
	require("plugins/nvim-treesitter"), -- Syntax highlighting, ... using treesitter
	require("plugins/nvim-ts-autotag"), -- Automatically close HTML tags
	require("plugins/oil"), -- File explorer
	-- require("plugins/precognition"), -- Show vim motions hints
	require("plugins/smear-cursor"), -- Animate cursor movement
	require("plugins/todo-comments"), -- Highlight TODO, FIXME, ...
	require("plugins/treesj"), -- Split/join lines with treesitter
	require("plugins/vim-dirtytalk"), -- Add various words to the spell checking dictionary
	require("plugins/vim-illuminate"), -- Highlight all instances of the word under the cursor
	require("plugins/virt-column"), -- Show a vertical line at position 80 ...
	require("plugins/visimatch"), -- Highlight text matching selection like VSCode
	require("plugins/vscode"), -- VSCode theme
	require("plugins/which-key"), -- Show possible keybindings
	"ThePrimeagen/vim-be-good", -- Vim keybinding training game
}, {})
