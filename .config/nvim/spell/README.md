# Spell files

Downloaded from https://ftp.nluug.nl/pub/vim/runtime/spell/

Nice tutorial: https://johncodes.com/posts/2023/02-25-nvim-spell/
Official docs: https://neovim.io/doc/user/spell.html
