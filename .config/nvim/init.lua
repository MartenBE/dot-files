-- Make sure to set $XDG_CONFIG_HOME to ~/.config
--
-- https://neovim.io/doc/user/lua-guide.html#lua-guide
--
-- Good defaults:
--     - https://github.com/nvim-lua/kickstart.nvim
--     - https://github.com/NvChad/NvChad
--     - https://github.com/LazyVim/LazyVim

-- These must be declared first so plugins know this (e.g. conform.nvim)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

require("options")

-- Download if necessary and execute lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end

-- Add lazy to the `runtimepath`, this allows us to `require` it.
vim.opt.rtp:prepend(lazypath)

require("lazy-plugins")
require("keymaps")
require("functions")
require("autocommands")

