local wezterm = require("wezterm")
local config = wezterm.config_builder()

-- GUI --------------------------------------------------------------------------

config.color_scheme = "SpaceGray Eighties"
config.colors = {
	-- https://github.com/igorkulman/wezterm-vscode-theme/blob/main/vscode.toml
	foreground = "#cccccc",
	background = "#1f1f1f",
}

-- config.font_size = 12.0
-- config.font = wezterm.font("JetBrains Mono")

config.font_size = 12.5
config.font = wezterm.font("Fira Code")

config.enable_scroll_bar = true

-- https://github.com/wez/wezterm/issues/6334
-- https://blurbusters.com/faq/benefits-of-frame-rate-above-refresh-rate/#:~:text=Running%20at%20frame%20rates%20much,%2Dbuffered%20modes%2C%20etc
config.max_fps = 120 --

-- Keys -------------------------------------------------------------------------

config.disable_default_key_bindings = true
config.leader = { key = "a", mods = "CTRL" }
config.keys = {
	-- https://wezfurlong.org/wezterm/config/keys.html
	-- https://wezfurlong.org/wezterm/config/default-keys.html
	-- https://wezfurlong.org/wezterm/config/lua/keyassignment/index.html
	-- Copy/Paste
	{ key = "c", mods = "CTRL|SHIFT", action = wezterm.action.CopyTo("Clipboard") },
	{ key = "v", mods = "CTRL|SHIFT", action = wezterm.action.PasteFrom("Clipboard") },
	{ key = "Insert", mods = "SHIFT", action = wezterm.action.PasteFrom("PrimarySelection") },
	-- Panes
	{ key = "v", mods = "LEADER", action = wezterm.action.SplitVertical },
	{ key = "s", mods = "LEADER", action = wezterm.action.SplitHorizontal },
	{ key = "c", mods = "LEADER", action = wezterm.action({ CloseCurrentPane = { confirm = true } }) },
	{ key = "h", mods = "LEADER", action = wezterm.action.ActivatePaneDirection("Left") },
	{ key = "j", mods = "LEADER", action = wezterm.action.ActivatePaneDirection("Down") },
	{ key = "k", mods = "LEADER", action = wezterm.action.ActivatePaneDirection("Up") },
	{ key = "l", mods = "LEADER", action = wezterm.action.ActivatePaneDirection("Right") },
	{ key = "w", mods = "LEADER", action = wezterm.action.ActivatePaneDirection("Next") },
	-- Resize font
	{ key = "+", mods = "CTRL", action = wezterm.action.IncreaseFontSize },
	{ key = "-", mods = "CTRL", action = wezterm.action.DecreaseFontSize },
	{ key = "0", mods = "CTRL", action = wezterm.action.ResetFontSize },
	-- Scroll
	{ key = "b", mods = "LEADER", action = wezterm.action.ScrollByPage(1) },
	{ key = "f", mods = "LEADER", action = wezterm.action.ScrollByPage(-1) },
	{ key = "d", mods = "LEADER", action = wezterm.action.ScrollByPage(0.5) },
	{ key = "u", mods = "LEADER", action = wezterm.action.ScrollByPage(-0.5) },
	{ key = "{", mods = "LEADER", action = wezterm.action.ScrollToPrompt(-1) },
	{ key = "}", mods = "LEADER", action = wezterm.action.ScrollToPrompt(1) },
	-- Tabs
	{ key = "t", mods = "LEADER", action = wezterm.action({ SpawnTab = "CurrentPaneDomain" }) },
	{ key = "p", mods = "LEADER", action = wezterm.action({ ActivateTabRelative = -1 }) },
	{ key = "n", mods = "LEADER", action = wezterm.action({ ActivateTabRelative = 1 }) },
	{ key = "1", mods = "LEADER", action = wezterm.action({ ActivateTab = 0 }) },
	{ key = "2", mods = "LEADER", action = wezterm.action({ ActivateTab = 1 }) },
	{ key = "3", mods = "LEADER", action = wezterm.action({ ActivateTab = 2 }) },
	{ key = "4", mods = "LEADER", action = wezterm.action({ ActivateTab = 3 }) },
	{ key = "5", mods = "LEADER", action = wezterm.action({ ActivateTab = 4 }) },
	{ key = "6", mods = "LEADER", action = wezterm.action({ ActivateTab = 5 }) },
	{ key = "7", mods = "LEADER", action = wezterm.action({ ActivateTab = 6 }) },
	{ key = "8", mods = "LEADER", action = wezterm.action({ ActivateTab = 7 }) },
	{ key = "9", mods = "LEADER", action = wezterm.action({ ActivateTab = -1 }) },
	-- Other
	{ key = "F1", mods = "LEADER", action = wezterm.action.ActivateCommandPalette },
	{ key = "r", mods = "LEADER", action = wezterm.action.ReloadConfiguration },
	{ key = "i", mods = "LEADER", action = wezterm.action.CharSelect },
	{ key = "/", mods = "LEADER", action = wezterm.action.Search({ CaseInSensitiveString = "" }) },
}

-- TODO: https://github.com/wez/wezterm/issues/6256

return config
